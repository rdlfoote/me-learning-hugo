# Me Learning Hugo

This is the git repo I created on my laptop for version controlling my first 
attempt at creating a website (with the exception of my senior project 11
years before) using Hugo. I'm sharing this project because the git log is the 
story of how I learned the basics of html and css and is hopefully a summary 
of how well I understand html and css, which is to say: I can technically use 
it.

This repo is also here for me to keep track of where I learned all the tricks 
that this website employs. I have a bad habit of not bookmarking helpful 
websites and Stack Overflow answers that answer very specific questions I have.
In this case, those are stored in the credits.

# Disclaimers

- This repo is informal and therefore not a good source for my writing style. 
  I wasn't expecting to share these commits, so they're badly written.

- This is not a Hugo theme. It might be made into a Hugo theme one day, but 
  it's missing a lot of polish.

# Credits:

- Thanks to [Kimberly Garmoe 
  (LinkedIn)](https://www.linkedin.com/in/kimberlygarmoe/) for telling me to 
  learn Hugo. Having a good framework picked out for me really helped me feel 
  less overwhelmed about creating a website.

- Thanks to [zwbetz's guide to Hugo from 
  scratch](https://zwbetz.com/make-a-hugo-blog-from-scratch/) for getting me 
  started. This should be the official quickstart guide to Hugo. The current 
  official Hugo quickstart guide uses theming, which didn't help me understand 
  how Hugo works. This guide did help me understand how it worked, and it did 
  so concisely. I will say that the inclusion of bootstrap only confused me, 
  but I suspect the intended audience is not people brand new to web 
  development.

- Thanks to [disinfor's Stack Overflow answer about popout 
  sidebars](https://stackoverflow.com/a/63863769) for helping me get the gist 
  of how to implement the side navigation bar that I was absolutely insistent 
  on making work that way.

- Thanks to [responders to this Stack Overflow
  question](https://stackoverflow.com/questions/22252472/how-to-change-the-color-of-an-svg-element)
  that taught me about `fill="currentColor"` for svg's.

- Thanks to [Mohammad Usman's Stack Overflow answer about aligning svg elements 
  with text](https://stackoverflow.com/a/37742207) for helping me make that 
  happen.

- Thanks to [1WD's post about javascript-free mobile nav 
  bars](https://1stwebdesigner.com/pure-css-navigation-menus/) for inspiring my 
  top navigation menu. I worked off of "Mobile Fade In Menu."

- I got all the icons from [svgrepo.com](https://www.svgrepo.com/). I just 
  learned that the licenses are listed for the icons, and I haven't 
  double-checked that the icons are attributed properly. I will do so as soon 
  as I can. Let me know if this concerns you.

- https://www.checkmyworking.com/cm-web-fonts/

- https://www.keycdn.com/blog/web-font-performance
